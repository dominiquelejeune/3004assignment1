package start;

import java.io.Console;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import server.AppServer;
import utils.Config;

@SuppressWarnings("unused")
public class StartServer {
	
	private static Boolean done = Boolean.FALSE;
	private static Boolean started = Boolean.FALSE;

	private static Scanner sc = new Scanner(System.in);
	private static AppServer appServer = null;
	
	static Logger logger = Logger.getLogger(StartServer.class.getName());
	
	public static void main(String[] argv) {

		System.out.println("Starting server ...");

		{
			System.out.println("Please enter in the number of player: ");
			String nop = sc.nextLine();
			Config.NUM_CLIENTS = Integer.parseInt(nop);
			
			logger.info(String.format("Number of players set to :%d",Config.NUM_CLIENTS));	
			
			System.out.println("Please enter in the number of rounds to play: ");
			String rtp = sc.nextLine();
			Config.NUM_OF_ROUNDS = Integer.parseInt(rtp);
			logger.info(String.format("Number of rounds to play set to :%d",Config.NUM_OF_ROUNDS));
		}

		do {
			System.out.println("startup | shutdown");
			String input = sc.nextLine();
			
			if (input.equalsIgnoreCase("STARTUP") || input.equalsIgnoreCase("START") && !started)
			{
				System.out.println("Starting server ...");
				appServer = new AppServer(Config.DEFAULT_PORT);
				started = Boolean.TRUE;
				logger.info(String.format("Server Started %s:%d", Config.DEFAULT_HOST, Config.DEFAULT_PORT));	
			}
			
			if (input.equalsIgnoreCase("SHUTDOWN") && started)
			{
				System.out.println("Shutting server down ...");
				appServer.shutdown();
				started = Boolean.FALSE;
				done = Boolean.TRUE;
				logger.info(String.format("Server Shutdown %s:%d", Config.DEFAULT_HOST, Config.DEFAULT_PORT));			
			}	
			
		} while (!done);

		System.out.println("Exiting ...");
		System.exit(1);
	}
}
