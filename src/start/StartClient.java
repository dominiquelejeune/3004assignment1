package start;

import client.AppClient;
import edu.carleton.scs.chat.utils.Config;

public final class StartClient {

	public static void main (String[] argv) {
		//new AppClient(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
		AppClient client = new AppClient();
		client.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
	}
}
