package example;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import example.RulesEngine;

import org.junit.Before;

public class TestRulesEngine {
	RulesEngine rEngine;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngine");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestRulesEngine");
		rEngine = new RulesEngine();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngine");
		rEngine = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngine");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    @Test
	public void WaitingState () {
    	System.out.println("@Test: WaitingState");
		rEngine.setState(RulesEngine.WAITING);
		assertEquals("Knock! Knock!", rEngine.processInput("Turnip"));
	}
	
    @Test
	public void Response () {
    	System.out.println("@Test: Response");
		rEngine.setState(RulesEngine.SENTKNOCKKNOCK);
		assertTrue("Knock! Knock!" != rEngine.processInput("Whats up?"));
		assertTrue("Turnip" == rEngine.processInput("Who's there?"));
	}

}