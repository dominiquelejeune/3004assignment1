/**
 * 
 * @author Howard Scott Needham
 * @Version 1.0
 * 
 * 1: open a server side socket/port and establish a connection 	(Constructor)
 * 2:	start the servers main thread to listen for client request 	(start/run methods)
 * 3:	The run method is a thread that listens for client requests. When a client
 * 	requests a connection it starts a new server thread that is dedicated to the
 * 	client and is then registered in the clients hashmap - addThread () method
 * 4:	The handle method is called from the servers client thread to handle the incoming message
 * 	Notice how we filter the message - this is where you would plug in your rules engine etc .....
 * 
 * 	The server thread listens on the assigned port and when it receives a message it passes it
 * 	back to the server for processing. It is usually best practice to handle this in a seperate
 * 	class that is dedicated to handling/processing messages
 */

package server;

import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import java.io.*;

import utils.Config;
import utils.Filter;
import utils.Formatter;

public class AppServer implements Runnable {
	static Logger logger = Logger.getLogger(AppServer.class.getName());
	
	int clientCount = 0;
	int roundCount = 1;
	
	private Filter filter;
	private Thread thread = null;
	private ServerSocket server = null;
	

	private Player[] players;
	private HashMap<Integer, ServerThread> clients;
	
	protected static Boolean gameStarted = Boolean.FALSE;
	protected static GameState gameState = GameState.SETUP;
	
	public AppServer(int port) {
		try {
			/** Set up our message filter object */
			filter = new Filter();
			
			System.out.println("Binding to port " + port + ", please wait  ...");
			logger.info("Binding to port " + port);
			
			/**
			 * I use a HashMap to keep track of the client connections and their
			 * related thread
			 */
			clients = new HashMap<Integer, ServerThread>();
			
			/** Establish the servers main port that it will listen on */
			server = new ServerSocket(port);
			/**
			 * Allows a ServerSocket to bind to the same address without raising
			 * an "already bind exception"
			 */
			logger.info(String.format("Setting number of players to %d\n",Config.NUM_CLIENTS));
			players = new Player[Config.NUM_CLIENTS];
			server.setReuseAddress(true);
			start();
		} catch (IOException ioe) {
			logger.fatal(Formatter.exception("Contructor", ioe));
		}
	}

	/** Now we start the servers main thread */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();			
			logger.info(String.format("Server started: %s %d", server,thread.getId()));
		}
	}

	/** The main server thread starts and is listening for clients to connect */
	public void run() {
		while (thread != null) {
			try {
				logger.info("Waiting for a client ...");
				addThread(server.accept());
			} catch (IOException ioe) {				
				logger.fatal(Formatter.exception("run()", ioe));
			}}
	}

	/** 
	 * Client connection is accepted and now we need to handle it and register it 
	 * and with the server | HashTable 
	 **/
	private void addThread(Socket socket) {
		logger.info(Formatter.format("Client Requesting connection: " , socket));
		
		if (clientCount < Config.NUM_CLIENTS) {
			try {
				/** Create a separate server thread for each client */
				ServerThread serverThread = new ServerThread(this, socket);
				/** Open and start the thread */
				serverThread.open();
				serverThread.start();
				
				serverThread.send(String.format("[%s:%d] Welcome to the game. Your port id is %s:%d\n", server.getInetAddress(), server.getLocalPort(), serverThread.getSocketAddress(), serverThread.getID()));				
				serverThread.send(String.format("[%s:%d] Please Enter in your user name:\n",serverThread.getSocketAddress(), serverThread.getID()));
				
				clients.put(serverThread.getID(), serverThread);		
				logger.info(Formatter.format("Client Accepted: " , socket));
				
				Player player = new Player(serverThread.getID());
				players[clientCount++] = player;
				
				serverThread.send("Welcome to the dark side: Please enter in your Jedi Name :\n");				
				//if (clientCount == Config.NUM_CLIENTS) 
				//	gameState = GameState.SET_ATTACK_DEFENSE_MOVES;

			} catch (IOException ioe) {
				logger.fatal(Formatter.exception("addThread(Socket)", ioe));
			}
		} else {
			logger.info(Formatter.format("Client Tried to connect:", socket));
			logger.info(Formatter.format("Client refused: maximum number of clients reached:", Config.NUM_CLIENTS));
		}
	}

	private int namesSet = 0;
	protected void setPlayersName (int portId, String name) {
		logger.info(String.format("Setting players name to %s on port id %d\n",name,portId));
		logger.info(String.format("Number of registered players          %d\n",players.length));
		if (namesSet < Config.NUM_CLIENTS) {
			for (Player p : players) {
				if (p!= null) {
					logger.info(String.format("Checking players port id %d\n",p.getPortId()));
					if (p.getPortId() == portId) {
						p.setName(name);
						logger.info(String.format("Players name set to %d %s\n",portId, p.getName()));
						ServerThread thread = clients.get(portId);			
						logger.info(String.format("Sending message back to player on port %d\n",thread.getID()));
						thread.send(String.format("Welcome to the game master %s\n",name));	
						namesSet += 1;
				}}}}
	
		if (namesSet ==Config.NUM_CLIENTS) {
			for (Entry<Integer,ServerThread> entity : clients.entrySet()) {
				ServerThread thread = entity.getValue();
				thread.send("Please enter in your attack/defense moves\n");
			}
			gameState = GameState.SET_ATTACK_DEFENSE_MOVES;	
		}
	}
	
	private int playerCount = 0;
	protected void setAttackDefenseMoves (int ID, String input) {
		
		if (input != null) {
			/** Second: get all players attack/defense moves */
			if (playerCount < Config.NUM_CLIENTS) {
				String opponentsName = input.split(" ")[0];
				
				Player attackPlayer = null; 
				Player opponentPlayer = null;
				
				for (Player p : players) {
					if (p.getPortId() == ID) {
						attackPlayer = p;
					}
					if (p.getName().equalsIgnoreCase(opponentsName)) {
						opponentPlayer = p;
				}}
	
				attackPlayer.setMove(opponentPlayer, input);
				playerCount++;
			}
		}
		
		System.err.println("Player Count " + playerCount);
		if (playerCount == Config.NUM_CLIENTS) {		
			/** Third: we need to get the players rolls */
			for (Map.Entry<Integer, ServerThread> entity : clients.entrySet()) {
				ServerThread thread = entity.getValue();
				thread.send("Roll to resolve:\n");
			}
			gameState = GameState.ROLL_TO_RESOLVE;
		}
	}
	
	private int rollsSet = 0;
	protected void setRolls (int ID, String input) {
		if (input != null) {
		if (rollsSet < Config.NUM_CLIENTS) {
			for (Player p : players) {
				if (p.getPortId() == ID) {
					p.setRoll(Integer.parseInt(input));
					rollsSet += 1;
				}}}}
		
		if (rollsSet == Config.NUM_CLIENTS) {			
			RulesEngine.process(players);		
			for (Map.Entry<Integer, ServerThread> entity : clients.entrySet()) {
				ServerThread thread = entity.getValue();
				for (Player p : players) {
					thread.send(p.getResult());
				}}
			if (roundCount == Config.NUM_OF_ROUNDS) {
			   gameState = gameState.FINISHED;
			   for (Map.Entry<Integer, ServerThread> entity : clients.entrySet()) {
					ServerThread thread = entity.getValue();
					for (Player p : players) {
						thread.send(String.format("Game Over, enter 'quit!'\n"));
					}}
			} else {
				gameState = gameState.SET_ATTACK_DEFENSE_MOVES;
				playerCount = 0;
				rollsSet = 0;
				for (Map.Entry<Integer, ServerThread> entity : clients.entrySet()) {
					ServerThread thread = entity.getValue();
					roundCount++;
					for (Player p : players) {
						thread.send(String.format("Starting round %d\n",roundCount));
					}}
			}	
		}
	}
	
	protected Boolean checkDisconnent (int ID, String input) {
		if (input.equals("quit!")) 
		{
			logger.info(Formatter.format("Removing Client:", ID));
			if (clients.containsKey(ID)) {
				clients.get(ID).send("quit!" + "\n");
				remove(ID);
				return Boolean.TRUE;
			}}
		return Boolean.FALSE;
	}
	
	protected Boolean checkShutdown (int ID, String input) {
		if (input.equals("shutdown!")) { shutdown();	}
		return Boolean.FALSE;
	}
	
	/** Try and shutdown the client cleanly */
	public synchronized void remove(int ID) {
		if (clients.containsKey(ID)) {
			ServerThread toTerminate = clients.get(ID);
			clients.remove(ID);
			clientCount--;

			toTerminate.close();
			toTerminate = null;
		}
	}

	/** Shutdown the server cleanly */
	public void shutdown() {
		Set<Integer> keys = clients.keySet();

		if (thread != null) {
			thread = null;
		}

		try {
			for (Integer key : keys) {
				clients.get(key).close();
			}
			clients.clear();
			server.close();
		} catch (IOException ioe) {
			logger.fatal(Formatter.exception("shutdown()", ioe));
		}
		logger.info(String.format("Server Shutdown cleanly %s", server));
	}
	
}
