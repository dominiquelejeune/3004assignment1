package server;

public enum GameState {
	SETUP, SET_ATTACK_DEFENSE_MOVES, ROLL_TO_RESOLVE, PROCESS, FINISHED;
}
