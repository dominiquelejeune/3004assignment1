package server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import server.RulesEngine;

import org.junit.Before;

public class TestRulesEngine {
	RulesEngine rEngine;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngine");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestRulesEngine");
		rEngine = new RulesEngine();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngine");
		rEngine = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngine");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    // Testing speeds: peter attacks with speed < jane's defense 
    @Test
	public void LesserSpeed() {
    	System.out.println("@Test: LesserSpeed");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 1 charge 3");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
    // Testing speeds: peter attacks with speed = jane's defense 
    @Test
	public void EqualSpeed() {
    	System.out.println("@Test: EqualSpeed");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
    // Testing speeds: peter attacks with speed > jane's defense 
    @Test
	public void GreaterSpeed() {
    	System.out.println("@Test: GreaterSpeed");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 3 charge 1");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
    // Testing attacks: if peter's thrust is correctly calculated with the roll of 1, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType1() {
    	System.out.println("@Test: AttackType1");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
    // Testing attacks: if peter's thrust is correctly calculated with the roll of 3, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType2() {
    	System.out.println("@Test: AttackType2");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(3);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
    // Testing attacks: if peter's thrust is correctly calculated with the roll of 5, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType3() {
    	System.out.println("@Test: AttackType3");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(5);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
    // Testing attacks: if peter's smash is correctly calculated with the roll of 2, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType4() {
    	System.out.println("@Test: AttackType4");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(2);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
    // Testing attacks: if peter's smash is correctly calculated with the roll of 4, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType5() {
    	System.out.println("@Test: AttackType5");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(4);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
    // Testing attacks: if peter's smash is correctly calculated with the roll of 6, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType6() {
    	System.out.println("@Test: AttackType6");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(6);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
    // Testing attacks: if peter's swing is correctly calculated with the roll of 1, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType7() {
    	System.out.println("@Test: AttackType7");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane swing 2 charge 2");
    	p2.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing attacks: if peter's swing is correctly calculated with the roll of 3, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType8() {
    	System.out.println("@Test: AttackType8");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane swing 2 charge 2");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(3);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing attacks: if peter's swing is correctly calculated with the roll of 5, jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void AttackType9() {
    	System.out.println("@Test: AttackType9");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane swing 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(5);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's thrust will beat jane's charge, so jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType1() {
    	System.out.println("@Test: DefenseType1");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's thrust will not beat jane's duck, so jane will not be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType2() {
    	System.out.println("@Test: DefenseType2");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's thrust will not beat jane's dodge, so jane will not be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType3() {
    	System.out.println("@Test: DefenseType3");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's smash will beat jane's duck, so jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType4() {
    	System.out.println("@Test: DefenseType4");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's smash will not beat jane's dodge, so jane will not be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType5() {
    	System.out.println("@Test: DefenseType5");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's smash will not beat jane's charge, so jane will not be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType6() {
    	System.out.println("@Test: DefenseType6");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's swing will beat jane's dodge, so jane will be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType7() {
    	System.out.println("@Test: DefenseType7");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane swing 2 charge 2");
    	p2.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's swing will not beat jane's charge, so jane will not be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType8() {
    	System.out.println("@Test: DefenseType8");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane swing 2 charge 2");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing defenses: if peter's swing will not beat jane's duck, so jane will not be wounded.
    // (the speeds are the same so that won't cause any wounds)
    @Test
	public void DefenseType9() {
    	System.out.println("@Test: DefenseType9");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane swing 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing when time and attack cause a hit.
    @Test
	public void SpeedAndAttackHit() {
    	System.out.println("@Test: SpeedAndAttackHit");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 1 charge 3");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing when neither time nor attack cause a hit.
    @Test
	public void NoHit() {
    	System.out.println("@Test: NoHit");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing a full round with 2 players and no wounds.
    @Test
	public void FullRoundNoHits() {
    	System.out.println("@Test: FullRoundNoHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 0 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing a full round with 2 players and peter is wounded.
    @Test
	public void FullRoundP1Hit() {
    	System.out.println("@Test: FullRoundP1Hit");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter thrust 2 duck 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
	}
    
 // Testing a full round with 2 players and jane is wounded.
    @Test
	public void FullRoundP2Hit() {
    	System.out.println("@Test: FullRoundP2Hit");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 0 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing a full round with 2 players and both are wounded.
    @Test
	public void FullRoundBothHit() {
    	System.out.println("@Test: FullRoundBothHit");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	p1.setName("peter");
    	p2.setName("jane");
    	p1.setMove(p2, "jane smash 1 charge 3");
    	p2.setMove(p1, "peter thrust 3 duck 1");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	Player[] players = { p1, p2 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
	}
    
 // Testing a full round with 3 players all with different targets and none are wounded.
    @Test
	public void ThreeDiffTargetsNoHits() {
    	System.out.println("@Test: ThreeDiffTargetsNoHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p3, "pat smash 2 duck 2");
    	p3.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	Player[] players = { p1, p2, p3 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 0 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
	}
    
 // Testing a full round with 3 players all with different targets and some are wounded.
    @Test
	public void ThreeDiffTargetsSomeHits() {
    	System.out.println("@Test: ThreeDiffTargetsSomeHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p3, "pat swing 2 duck 2");
    	p3.setMove(p1, "peter thrust 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	Player[] players = { p1, p2, p3 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 1 time(s)\n", p3.getResult());
	}
    
 // Testing a full round with 3 players all with different targets and all are wounded.
    @Test
	public void ThreeDiffTargetsAllHits() {
    	System.out.println("@Test: ThreeDiffTargetsAllHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p3, "pat swing 2 duck 2");
    	p3.setMove(p1, "peter thrust 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	Player[] players = { p1, p2, p3 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 1 time(s)\n", p3.getResult());
	}
    
 // Testing a full round with 3 players, 2 targeting the same player, and none are wounded.
    @Test
	public void ThreeSameTargetsNoHits() {
    	System.out.println("@Test: ThreeSameTargetsNoHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter smash 2 duck 2");
    	p3.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	Player[] players = { p1, p2, p3 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 0 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
	}
    
 // Testing a full round with 3 players, 2 targeting the same player, and one is wounded once.
    @Test
	public void ThreeSameTargetsOneHit() {
    	System.out.println("@Test: ThreeSameTargetsOneHit");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter thrust 2 duck 2");
    	p3.setMove(p1, "peter swing 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	Player[] players = { p1, p2, p3 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
	}
    
 // Testing a full round with 3 players, 2 targeting the same player, and one is wounded twice.
    @Test
	public void ThreeSameTargetsTwoHits() {
    	System.out.println("@Test: ThreeSameTargetsTwoHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter thrust 2 duck 2");
    	p3.setMove(p1, "peter thrust 2 dodge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	Player[] players = { p1, p2, p3 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 2 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
	}
    
 // Testing a full round with 4 players all with different targets and none are wounded.
    @Test
	public void FourDiffTargetsNoHits() {
    	System.out.println("@Test: FourDiffTargetsNoHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p3, "pat smash 2 duck 2");
    	p3.setMove(p4, "mary swing 2 dodge 2");
    	p4.setMove(p1, "peter smash 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 0 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 0 time(s)\n", p4.getResult());
	}
    
 // Testing a full round with 4 players all with different targets and some are wounded.
    @Test
	public void FourDiffTargetsSomeHits() {
    	System.out.println("@Test: FourDiffTargetsSomeHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p3, "pat swing 2 duck 2");
    	p3.setMove(p4, "mary swing 2 dodge 2");
    	p4.setMove(p1, "peter thrust 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 1 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 0 time(s)\n", p4.getResult());
	}
    
 // Testing a full round with 4 players all with different targets and all are wounded.
    @Test
	public void FourDiffTargetsAllHits() {
    	System.out.println("@Test: FourDiffTargetsAllHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p3, "pat swing 2 duck 2");
    	p3.setMove(p4, "mary thrust 2 dodge 2");
    	p4.setMove(p1, "peter thrust 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 1 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 1 time(s)\n", p4.getResult());
	}
    
 // Testing a full round with 4 players, 2 on the same target, and some are wounded.
    // It doesn't say to do multiple tests for this one so I'm assuming one is enough.
    @Test
	public void FourTwoOnOneSomeHits() {
    	System.out.println("@Test: FourTwoOnOneSomeHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane smash 2 charge 2");
    	p2.setMove(p3, "pat swing 2 duck 2");
    	p3.setMove(p1, "peter thrust 2 dodge 2");
    	p4.setMove(p1, "peter thrust 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 2 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 1 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 1 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 0 time(s)\n", p4.getResult());
	}
    
 // Testing a full round with 4 players, 3 on the same target, and none are wounded.
    @Test
	public void FourSameTargetsNoHits() {
    	System.out.println("@Test: FourSameTargetsNoHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p3.setMove(p1, "peter smash 2 dodge 2");
    	p4.setMove(p1, "peter swing 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 0 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 0 time(s)\n", p4.getResult());
	}
        
     // Testing a full round with 4 players, 3 on the same target, and it is wounded once.
    @Test
	public void FourSameTargetsOneHit() {
    	System.out.println("@Test: FourSameTargetsOneHit");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p3.setMove(p1, "peter smash 2 dodge 2");
    	p4.setMove(p1, "peter thrust 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 1 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 0 time(s)\n", p4.getResult());
	}
    
 // Testing a full round with 4 players, 3 on the same target, and it is wounded twice.
    @Test
	public void FourSameTargetsTwoHits() {
    	System.out.println("@Test: FourSameTargetsTwoHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter swing 2 duck 2");
    	p3.setMove(p1, "peter thrust 2 dodge 2");
    	p4.setMove(p1, "peter thrust 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 2 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 0 time(s)\n", p4.getResult());
	}
    
 // Testing a full round with 4 players, 3 on the same target, and it is wounded thrice.
    @Test
	public void FourSameTargetsThreeHits() {
    	System.out.println("@Test: FourSameTargetsThreeHits");
    	Player p1 = new Player(4004);
    	Player p2 = new Player(4005);
    	Player p3 = new Player(4006);
    	Player p4 = new Player(4007);
    	p1.setName("peter");
    	p2.setName("jane");
    	p3.setName("pat");
    	p4.setName("mary");
    	p1.setMove(p2, "jane thrust 2 charge 2");
    	p2.setMove(p1, "peter thrust 2 duck 2");
    	p3.setMove(p1, "peter thrust 2 dodge 2");
    	p4.setMove(p1, "peter thrust 2 charge 2");
    	p1.setRoll(1);
    	p2.setRoll(1);
    	p3.setRoll(1);
    	p4.setRoll(1);
    	Player[] players = { p1, p2, p3, p4 }; 
		rEngine.process(players);
		assertEquals("peter was wounded 3 time(s)\n", p1.getResult());
		assertEquals("jane was wounded 0 time(s)\n", p2.getResult());
		assertEquals("pat was wounded 0 time(s)\n", p3.getResult());
		assertEquals("mary was wounded 0 time(s)\n", p4.getResult());
	}

}