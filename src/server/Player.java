package server;

public class Player {

	String name;
	Integer portId;

	Player opponent;
	
	Boolean isReady = Boolean.FALSE;
	
	String AttackMove;
	String DefenseMove;
	
	Integer AttackSpeed;
	Integer DefenseSpeed;
	
	Integer roll;
	Integer wounds = 0;
	
	public Player (int portId) {
		this.portId = portId;
	}

	public Integer getPortId() {
		return portId;
	}

	public void setPortId(Integer portId) {
		this.portId = portId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Player getOpponent() {
		return opponent;
	}

	public Boolean getIsReady() {
		return isReady;
	}

	public String getAttackMove() {
		return AttackMove;
	}
	
	public void setAttackMove(String s) {
		AttackMove = s;
	}

	public String getDefenseMove() {
		return DefenseMove;
	}

	public Integer getAttackSpeed() {
		return AttackSpeed;
	}

	public Integer getDefenseSpeed() {
		return DefenseSpeed;
	}

	public Integer getRoll() {
		return roll;
	}

	public void setRoll(Integer roll) {
		this.roll = roll;
	}

	// player to Attack | Attack Move | Attack Speed | Defense Move | Defense Speed
	public void setMove (Player opponent, String input) {
		//System.out.println(">>>\t" + input);
		
		String[] tokens = input.split(" ");
		
		
		this.opponent = opponent;
		this.AttackMove = tokens[1];
		this.AttackSpeed = Integer.parseInt(tokens[2]);		
		this.DefenseMove = tokens[3];
		this.DefenseSpeed = Integer.parseInt(tokens[4]);
		
	}
	
	String result;
	public void setResult (String result) {
		this.result = result;
	}
	
	public void setWounded () {
		this.wounds += 1;
	}
	
	public String getResult () {
		//return String.format("%s attacked %s : won attack %s : number of wounds %d\n", name, opponent.getName(), result, wounds );
		return String.format("%s was wounded %d time(s)\n", name, wounds );
	}
}
