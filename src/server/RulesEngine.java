package server;

import org.apache.log4j.Logger;

public final class RulesEngine {
	//static Logger logger = Logger.getLogger(RulesEngine.class.getName());
	
	public static void process (Player[] players) {
		for (Player p : players) {
			processAttack (p, p.getOpponent());
		}
	}
	
	private static Boolean processAttack ( Player attacker, Player defender) {
		if (attacker.getRoll() == 1 || attacker.getRoll() == 2) {
			// Do nothing
		} else if (attacker.getRoll() == 3 || attacker.getRoll() == 4) {
			if (attacker.getAttackMove().equalsIgnoreCase("thrust")) {
				attacker.setAttackMove("smash");
			} else if (attacker.getAttackMove().equalsIgnoreCase("smash")) {
				attacker.setAttackMove("swing");
			} else if (attacker.getAttackMove().equalsIgnoreCase("swing")) {
				attacker.setAttackMove("thrust");
			}
		} else if (attacker.getRoll() == 5 || attacker.getRoll() == 6) {
			if (attacker.getAttackMove().equalsIgnoreCase("thrust")) {
				attacker.setAttackMove("swing");
			} else if (attacker.getAttackMove().equalsIgnoreCase("smash")) {
				attacker.setAttackMove("thrust");
			} else if (attacker.getAttackMove().equalsIgnoreCase("swing")) {
				attacker.setAttackMove("smash");
			}
		}
		
		//System.out.println(attacker.getName() + " >> " + defender.getName());
		if (attacker.getAttackSpeed() < defender.getDefenseSpeed()) {
			defender.setWounded();
		} else if (attacker.getAttackMove().equalsIgnoreCase("thrust") == true &&
				   defender.getDefenseMove().equalsIgnoreCase("charge") == true) {
			defender.setWounded();
		} else if (attacker.getAttackMove().equalsIgnoreCase("smash") == true &&
				   defender.getDefenseMove().equalsIgnoreCase("duck") == true) {
			defender.setWounded();
		} else if (attacker.getAttackMove().equalsIgnoreCase("swing") == true &&
				   defender.getDefenseMove().equalsIgnoreCase("dodge") == true) {
			defender.setWounded();
		}

		//attacker.setResult("");
		//defender.setResult("");
		return Boolean.TRUE;
	}
	
}
