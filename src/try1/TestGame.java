package try1;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;

public class TestGame {
	Game game;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestGame");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestGame");
		game = new Game(4, 1);
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestGame");
		game = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestGame");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    // Test adding a player 
    /*@Test
	public void JoinState() {
    	System.out.println("@Test: JoinState");
    	game.handleInput("join jane 209.169.146.161");
    	assertNotNull(game.getPlayer("jane"));
    	assertEquals("jane", game.getPlayer("jane").getName());
    	assertEquals("209.169.146.161", game.getPlayer("jane").getIPAddr());
	}
    
    // Functionality item 1: test selecting an attack 
    @Test
	public void AttackState() {
    	System.out.println("@Test: AttackState");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 1 duck 3");
    	assertEquals("joe", game.getPlayer("jack").getTarget());
    	assertEquals(2, game.getPlayer("jack").getAttack());
    	assertEquals(1, game.getPlayer("jack").getAttackSpd());
    	assertEquals(1, game.getPlayer("jack").getDefense());
    	assertEquals(3, game.getPlayer("jack").getDefenseSpd());
	}
    
    // Functionality item 2a test 1: jack attacks with speed > joe's defense 
    @Test
	public void RollSpeed1() {
    	System.out.println("@Test: RollSpeed1");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 3 duck 1");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2a test 2: jack attacks with speed = joe's defense 
    @Test
	public void RollSpeed2() {
    	System.out.println("@Test: RollSpeed2");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2a test 3: jack attacks with speed < joe's defense 
    @Test
	public void RollSpeed3() {
    	System.out.println("@Test: RollSpeed3");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 1 duck 3");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(1, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2b test 1: jack attacks with thrust and rolls a 1 
    @Test
	public void AttackType1() {
    	System.out.println("@Test: AttackType1");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe thrust 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	// The game stores that attack and defenses position in an array so thrust is 0
    	assertEquals(0, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 2: jack attacks with thrust and rolls a 3 
    @Test
	public void AttackType2() {
    	System.out.println("@Test: AttackType2");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe thrust 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 3");
    	// The game stores that attack and defenses position in an array so smash is 1
    	assertEquals(1, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 3: jack attacks with thrust and rolls a 5 
    @Test
	public void AttackType3() {
    	System.out.println("@Test: AttackType3");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe thrust 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 5");
    	// The game stores that attack and defenses position in an array so swing is 2
    	assertEquals(2, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 4: jack attacks with smash and rolls a 2 
    @Test
	public void AttackType4() {
    	System.out.println("@Test: AttackType4");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe smash 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 2");
    	// The game stores that attack and defenses position in an array so smash is 1
    	assertEquals(1, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 5: jack attacks with smash and rolls a 4 
    @Test
	public void AttackType5() {
    	System.out.println("@Test: AttackType5");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe smash 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 4");
    	// The game stores that attack and defenses position in an array so swing is 2
    	assertEquals(2, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 6: jack attacks with smash and rolls a 6 
    @Test
	public void AttackType6() {
    	System.out.println("@Test: AttackType6");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe smash 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 6");
    	// The game stores that attack and defenses position in an array so thrust is 0
    	assertEquals(0, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 7: jack attacks with swing and rolls a 1 
    @Test
	public void AttackType7() {
    	System.out.println("@Test: AttackType7");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	// The game stores that attack and defenses position in an array so swing is 2
    	assertEquals(2, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 8: jack attacks with swing and rolls a 3 
    @Test
	public void AttackType8() {
    	System.out.println("@Test: AttackType8");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 3");
    	// The game stores that attack and defenses position in an array so thrust is 0
    	assertEquals(0, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2b test 9: jack attacks with swing and rolls a 5 
    @Test
	public void AttackType9() {
    	System.out.println("@Test: AttackType9");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 5");
    	// The game stores that attack and defenses position in an array so smash is 1
    	assertEquals(1, game.getPlayer("jack").getAttack());
	}
    
    // Functionality item 2c test 1: jack attacks with thrust against joe's charge defense 
    @Test
	public void DefenseType1() {
    	System.out.println("@Test: DefenseType1");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe thrust 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(1, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 2: jack attacks with thrust against joe's duck defense 
    @Test
	public void DefenseType2() {
    	System.out.println("@Test: DefenseType2");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe thrust 2 duck 2");
    	game.handleInput("select joe jack thrust 2 duck 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 3: jack attacks with thrust against joe's dodge defense 
    @Test
	public void DefenseType3() {
    	System.out.println("@Test: DefenseType3");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe thrust 2 duck 2");
    	game.handleInput("select joe jack thrust 2 dodge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 4: jack attacks with smash against joe's charge defense 
    @Test
	public void DefenseType4() {
    	System.out.println("@Test: DefenseType4");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe smash 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 5: jack attacks with smash against joe's duck defense 
    @Test
	public void DefenseType5() {
    	System.out.println("@Test: DefenseType5");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe smash 2 duck 2");
    	game.handleInput("select joe jack thrust 2 duck 2");
    	game.handleInput("roll jack 1");
    	assertEquals(1, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 6: jack attacks with smash against joe's dodge defense 
    @Test
	public void DefenseType6() {
    	System.out.println("@Test: DefenseType6");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe smash 2 duck 2");
    	game.handleInput("select joe jack thrust 2 dodge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 7: jack attacks with swing against joe's charge defense 
    @Test
	public void DefenseType7() {
    	System.out.println("@Test: DefenseType7");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 2 duck 2");
    	game.handleInput("select joe jack thrust 2 charge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 8: jack attacks with swing against joe's duck defense 
    @Test
	public void DefenseType8() {
    	System.out.println("@Test: DefenseType8");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 2 duck 2");
    	game.handleInput("select joe jack thrust 2 duck 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2c test 9: jack attacks with swing against joe's dodge defense 
    @Test
	public void DefenseType9() {
    	System.out.println("@Test: DefenseType9");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 2 duck 2");
    	game.handleInput("select joe jack thrust 2 dodge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(1, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2d: jack hits joe due to speed and attack 
    @Test
	public void Hit() {
    	System.out.println("@Test: Hit");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe swing 1 duck 3");
    	game.handleInput("select joe jack thrust 2 dodge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(1, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 2d: jack's attack misses joe 
    @Test
	public void NoHit() {
    	System.out.println("@Test: NoHit");
    	game.handleInput("join jack 209.169.146.161");
    	game.handleInput("join joe 209.169.146.178");
    	game.handleInput("select jack joe smash 3 duck 1");
    	game.handleInput("select joe jack thrust 2 dodge 2");
    	game.handleInput("roll jack 1");
    	assertEquals(0, game.getPlayer("joe").getWounds());
	}
    
    // Functionality item 3: Both attacks miss
    @Test
	public void FullRoundNoHits() {
    	System.out.println("@Test: FullRoundNoHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("select jack joe smash 2 duck 2");
    	output = game.handleInput("select joe jack thrust 2 dodge 2");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 3: jack's attack hits joe
    @Test
	public void FullRoundP1Hit() {
    	System.out.println("@Test: FullRoundP1Hit");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("select jack joe swing 1 duck 3");
    	output = game.handleInput("select joe jack thrust 3 dodge 1");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(1, game.getPlayer("joe").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 3: joe's attack hits jack
    @Test
	public void FullRoundP2Hit() {
    	System.out.println("@Test: FullRoundP2Hit");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("select jack joe thrust 1 charge 3");
    	output = game.handleInput("select joe jack thrust 3 dodge 1");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(1, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 3: Both player's attacks hit
    @Test
	public void FullRoundBothHit() {
    	System.out.println("@Test: FullRoundBothHit");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("select jack joe swing 1 duck 3");
    	output = game.handleInput("select joe jack thrust 2 dodge 2");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(1, game.getPlayer("jack").getWounds());
    		assertEquals(1, game.getPlayer("joe").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}

    // Functionality item 4: A game with three players all targeting different players and no hits
    @Test
	public void ThreeDiffTargetsNoHits() {
    	System.out.println("@Test: ThreeDiffTargetsNoHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("select jack joe smash 2 duck 2");
    	output = game.handleInput("select joe jill thrust 2 dodge 2");
    	output = game.handleInput("select jill jack swing 2 duck 2");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with three players all targeting different players and some hits
    @Test
	public void ThreeDiffTargetsSomeHits() {
    	System.out.println("@Test: ThreeDiffTargetsSomeHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("select jack joe swing 2 duck 2");
    	output = game.handleInput("select joe jill thrust 2 dodge 2");
    	output = game.handleInput("select jill jack swing 2 charge 2");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(1, game.getPlayer("joe").getWounds());
    		assertEquals(1, game.getPlayer("jill").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with three players all targeting different players and all hits
    @Test
	public void ThreeDiffTargetsAllHits() {
    	System.out.println("@Test: ThreeDiffTargetsAllHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("select jack joe swing 2 duck 2");
    	output = game.handleInput("select joe jill thrust 2 dodge 2");
    	output = game.handleInput("select jill jack smash 2 charge 2");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(1, game.getPlayer("jack").getWounds());
    		assertEquals(1, game.getPlayer("joe").getWounds());
    		assertEquals(1, game.getPlayer("jill").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with three players, two targeting the same player and no hits
    @Test
	public void ThreeSameTargetsNoHits() {
    	System.out.println("@Test: ThreeSameTargetsNoHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("select jack joe swing 2 dodge 2");
    	output = game.handleInput("select joe jack thrust 2 duck 2");
    	output = game.handleInput("select jill jack smash 2 charge 2");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with three players, two targeting the same player and one hit
    @Test
	public void ThreeSameTargetsOneHit() {
    	System.out.println("@Test: ThreeSameTargetsOneHit");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("select jack joe swing 2 dodge 2");
    	output = game.handleInput("select joe jack swing 2 duck 2");
    	output = game.handleInput("select jill jack smash 2 charge 2");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(1, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with three players, two targeting the same player and two hits
    @Test
	public void ThreeSameTargetsTwoHits() {
    	System.out.println("@Test: ThreeSameTargetsTwoHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("select jack joe swing 2 dodge 2");
    	output = game.handleInput("select joe jack swing 2 duck 2");
    	output = game.handleInput("select jill jack smash 1 charge 3");
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(2, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with four players all targeting different players and no hits
    @Test
	public void FourDiffTargetsNoHits() {
    	System.out.println("@Test: FourDiffTargetsNoHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("join jane 209.169.146.134");
    	
    	output = game.handleInput("select jack joe smash 2 duck 2");
    	output = game.handleInput("select joe jill thrust 2 dodge 2");
    	output = game.handleInput("select jill jane swing 2 duck 2");
    	output = game.handleInput("select jane jack swing 2 charge 2");
    	
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	output = game.handleInput("roll jane 1");
    	
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    		assertEquals(0, game.getPlayer("jane").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with four players all targeting different players and some hits
    @Test
	public void FourDiffTargetsSomeHits() {
    	System.out.println("@Test: FourDiffTargetsSomeHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("join jane 209.169.146.134");
    	
    	output = game.handleInput("select jack joe smash 3 charge 1");
    	output = game.handleInput("select joe jill swing 2 dodge 2");
    	output = game.handleInput("select jill jane thrust 1 duck 3");
    	output = game.handleInput("select jane jack swing 2 duck 2");
    	
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	output = game.handleInput("roll jane 1");
    	
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(1, game.getPlayer("jill").getWounds());
    		assertEquals(1, game.getPlayer("jane").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with four players all targeting different players and all hits
    @Test
	public void FourDiffTargetsAllHits() {
    	System.out.println("@Test: FourDiffTargetsAllHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("join jane 209.169.146.134");
    	
    	output = game.handleInput("select jack joe smash 2 dodge 2");
    	output = game.handleInput("select joe jill swing 2 duck 2");
    	output = game.handleInput("select jill jane thrust 2 dodge 2");
    	output = game.handleInput("select jane jack swing 2 charge 2");
    	
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	output = game.handleInput("roll jane 1");
    	
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(1, game.getPlayer("jack").getWounds());
    		assertEquals(1, game.getPlayer("joe").getWounds());
    		assertEquals(1, game.getPlayer("jill").getWounds());
    		assertEquals(1, game.getPlayer("jane").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}*/
    
    // * * * * * * * * * * * * * * * * * * * * * * 
    // FOUR PLAYERS, TWO ON THE SAME TARGET?????????
    // * * * * * * * * * * * * * * * * * * * * * *
    
	// Functionality item 4: A game with four players, three targeting the same player and no hits
    @Test
	public void FourSameTargetsNoHits() {
    	System.out.println("@Test: FourSameTargetsNoHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("join jane 209.169.146.134");
    	
    	output = game.handleInput("select jack joe thrust 2 charge 2");
    	output = game.handleInput("select joe jack swing 2 duck 2");
    	output = game.handleInput("select jill jack smash 2 dodge 2");
    	output = game.handleInput("select jane jack swing 2 charge 2");
    	
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	output = game.handleInput("roll jane 1");
    	
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(0, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    		assertEquals(0, game.getPlayer("jane").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with four players, three targeting the same player and one hit
    @Test
	public void FourSameTargetsOneHit() {
    	System.out.println("@Test: FourSameTargetsOneHit");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("join jane 209.169.146.134");
    	
    	output = game.handleInput("select jack joe thrust 2 charge 2");
    	output = game.handleInput("select joe jack swing 2 duck 2");
    	output = game.handleInput("select jill jack smash 2 dodge 2");
    	output = game.handleInput("select jane jack thrust 2 charge 2");
    	
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	output = game.handleInput("roll jane 1");
    	
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(1, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    		assertEquals(0, game.getPlayer("jane").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
    // Functionality item 4: A game with four players, three targeting the same player and two hits
    @Test
	public void FourSameTargetsTwoHits() {
    	System.out.println("@Test: FourSameTargetsTwoHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("join jane 209.169.146.134");
    	
    	output = game.handleInput("select jack joe thrust 2 charge 2");
    	output = game.handleInput("select joe jack swing 2 duck 2");
    	output = game.handleInput("select jill jack thrust 2 dodge 2");
    	output = game.handleInput("select jane jack thrust 2 charge 2");
    	
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	output = game.handleInput("roll jane 1");
    	
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(2, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    		assertEquals(0, game.getPlayer("jane").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
    
 // Functionality item 4: A game with four players, three targeting the same player and three hits
    @Test
	public void FourSameTargetsThreeHits() {
    	System.out.println("@Test: FourSameTargetsThreeHits");
    	String output;
    	output = game.handleInput("join jack 209.169.146.161");
    	output = game.handleInput("join joe 209.169.146.178");
    	output = game.handleInput("join jill 209.169.146.134");
    	output = game.handleInput("join jane 209.169.146.134");
    	
    	output = game.handleInput("select jack joe thrust 2 charge 2");
    	output = game.handleInput("select joe jack thrust 2 duck 2");
    	output = game.handleInput("select jill jack thrust 2 dodge 2");
    	output = game.handleInput("select jane jack thrust 2 charge 2");
    	
    	output = game.handleInput("roll jack 1");
    	output = game.handleInput("roll joe 1");
    	output = game.handleInput("roll jill 1");
    	output = game.handleInput("roll jane 1");
    	
    	if (output != null) {
    		System.out.println(output);
    		assertEquals(3, game.getPlayer("jack").getWounds());
    		assertEquals(0, game.getPlayer("joe").getWounds());
    		assertEquals(0, game.getPlayer("jill").getWounds());
    		assertEquals(0, game.getPlayer("jane").getWounds());
    	}
    	if (game.getState() == Game.GAME_OVER) {
    		System.out.println("GAME OVER");
    	}
	}
}

