package try1;

import network.AppClient;
import utils.Config;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Before;

public class TestClient {
	AppClient client1, client2;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestClient");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestClient");
		//client = new AppClient(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
		client1 = new AppClient();
		client2 = new AppClient();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestClient");
		//client1 = null;
		client2 = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestClient");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
	
    // Test getting a connection to the server
    // code NEEDS to wait for connection to be properly set up and connected message from the server received.
    // it also needs to disconnect from the server properly
    @Test
	public void Connect() {
    	System.out.println("@Test: Connect");
		boolean result = client1.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
		assertEquals(true, result);
		client1.handle("quit!"); 
	}
    
    // Test connecting multiple clients
    @Test
	public void testMultipleConnections() {
    	System.out.println("@Test: MultipleClients");
		boolean result1 = client1.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
		boolean result2 = client2.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT);
		assertEquals(true, result1);
		assertEquals(true, result2);
		client1.handle("quit!");
		client2.handle("quit!");
	}
    
    // Test that the clients and server can talk properly
        
    // 

}