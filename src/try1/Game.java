package try1;

import java.util.*;

public class Game {

	public static final int JOIN = 0;
    public static final int ATTACK = 1;
    public static final int ROLL = 2;
    public static final int GAME_OVER = 3;
    public static final String[] attacklist = {"thrust", "smash", "swing"};
    public static final String[] defenselist = {"charge", "duck", "dodge"};

    private int state = JOIN;
    
    private HashMap<String, Player> players;
    private int numPlayers;
    private int maxPlayers;
    private int numRnds;
    private int currRnd;
    
	public Game(int num, int rnds) {
		players = new HashMap<String, Player>();
		maxPlayers = num;
		numPlayers = 0;
		numRnds = rnds;
		currRnd = 1;
	}
	
	public Player getPlayer(String name) {
		return players.get(name);
	}
	
	public int getState() {
		return state;
	}
	
	public boolean addPlayer(String name, String IP) {
		Player p = new Player(name, IP);
		players.put(name, p);
		return true;
	}
	
	public String handleInput(String input) {
		input.toLowerCase();
		String[] parts = input.split(" ");
		String output = null;
		
		
		if (parts[0].equals("join") && state == JOIN) {
			/* add the player to the game */
			addPlayer(parts[1], parts[2]);
			numPlayers++;
			/* if all players have submitted, proceed to the next state */
			if (numPlayers == maxPlayers) {
				state = ATTACK;
				numPlayers = 0;
				output = "The game has started, please select your moves in the form: %n"
						+ "select player target attack attackspeed defense defense speed";
			}
			
			
		} else if (parts[0].equals("select") && state == ATTACK) {
			/* add the player's move to the game */
			Player p = players.get(parts[1]);
			int a = -1, d = -1;
			for (int i=0; i < attacklist.length; i++) {
				if (attacklist[i].equals(parts[3])) { a = i; }
			}
			for (int j=0; j < defenselist.length; j++) {
				if (defenselist[j].equals(parts[5])) { d = j; }
			}
			
			p.setInfo(parts[2], a, Integer.parseInt(parts[4]), d, Integer.parseInt(parts[6]));
			numPlayers++;
			/* if all players have submitted, proceed to the next state */
			if (numPlayers == maxPlayers) {
				state = ROLL;
				numPlayers = 0;
				output = "All players have entered their moves, please enter your rolls in the form: %n"
						+ "roll player number";
			}
			
		} else if (parts[0].equals("roll") && state == ROLL) {
			/* modify the player's move depending on what the player rolled */
			int roll = Integer.parseInt(parts[2]);
			int incr;
			if (roll == 1 || roll == 2) {
				incr = 0;
			} else if (roll == 3 || roll == 4) {
				incr = 1;
			} else { //roll == 5 || roll == 6
				incr = 2;
			}
			Player p = players.get(parts[1]);
			p.setAttack((p.getAttack() + incr) % 3);
			
			Player t = players.get(p.getTarget());
			
			if (p.getAttack() == t.getDefense() || p.getAttackSpd() < t.getDefenseSpd()) {
				t.addWound();
			}
			numPlayers++;
			/* if all players have submitted, proceed to the next state */
			if (numPlayers == maxPlayers) {
				output = calculateRoundResult();
				if (currRnd < numRnds) {
					state = ATTACK;
					numPlayers = 0;
					currRnd++;
				} else {
					state = GAME_OVER;
				}
			}
			
		} else {
			output = "Error: command '" + input + "' not processed.";
		}
		
		return output;
	}
	
	public String calculateRoundResult() {
		String output = "Results for round " + currRnd + ":";
		for (String key : players.keySet()) {
		    output = output + "\n" + key + " has " + players.get(key).getWounds() + " wound(s).";
		}
		return output;
	}
}