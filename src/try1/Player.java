package try1;

public class Player {
	private String name;
	private String ipAddr;
	private String target;
	private int attack;
	private int attackSpd;
	private int defense;
	private int defenseSpd;
	private int wounds;
	
	public Player(String n, String IP) {
		name = n;
		ipAddr = IP;
		wounds = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIPAddr() {
		return ipAddr;
	}
	
	public String getTarget() {
		return target;
	}
	
	public int getAttack() {
		return attack;
	}
	
	public int getAttackSpd() {
		return attackSpd;
	}
	
	public int getDefense() {
		return defense;
	}
	
	public int getDefenseSpd() {
		return defenseSpd;
	}
	
	public int getWounds() {
		return wounds;
	}
	
	public void setAttack(int a) {
		attack = a;
	}
	
	public void addWound() {
		wounds++;
	}
	
	public void setInfo(String t, int a, int aSpd, int d, int dSpd) {
		target = t;
		attack = a;
		attackSpd = aSpd;
		defense = d;
		defenseSpd = dSpd;
	}
}
