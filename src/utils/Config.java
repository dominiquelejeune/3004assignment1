package utils;

public class Config {
	public static int NUM_CLIENTS = 5;
	public static int NUM_OF_ROUNDS = 2;
	
	public static final int DEFAULT_PORT = 5050;
	public static final String DEFAULT_HOST = "127.0.0.1";
	public static final String DEFAULT_USER = "Ducky";
	public static final boolean PRINT_STACK_TRACE = false;   
}
