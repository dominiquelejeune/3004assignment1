package utils;

import server.ServerThread;

public class RulesFormatter {
	
	/**
		There	are three possible attack	moves: Thrust,	Swing, Smash.	
		There	are three possible defense	moves: Charge,	Dodge, Duck.	
		
		Selecting an attack consists of selecting	an	attack move	and a	speed
			--- which ranges from 1	to	3.	
		Selecting a defense consists of selecting a defense move and a speed
			---	which	ranges from	1 to 3.	
		The sum of the	attack speed and defense speed must	be	at	least	4.
	**/
	
	/** The format will be */
	//Attackers Name v.s. Defenders Name
	//ipaddress:port -> Attachers Name: Roll : Attackers Move
	//ipaddress:port -> Defenders Name: Roll : Defenders Move
	//Result 
	
	/** Note there are four lines to each attack sequence */
	
	private String attacker;
	private String defender;
	
	public String getCombat (String attacker, String defender) {
		return String.format("\ns vs. %s",attacker, defender);
	}
	
	public String getAttacker (ServerThread thread, String name, String move, int roll) {
		return String.format("ATTACKER %s:d -> %s: %d : %s", thread.getSocketAddress(), thread.getId(), roll);
	}
	
	public String getDefender (ServerThread thread, String name, String move, int roll) {
		return String.format("DEFENDER %s:d -> %s: %d : %s", thread.getSocketAddress(), thread.getId(), roll);
	}	
	
	/** This is up to how who define the rules engine */
	/** You may modify this to match your rules and results */
	public String getResult (String result) {
		return String.format("%s\n",result);
	}
}
