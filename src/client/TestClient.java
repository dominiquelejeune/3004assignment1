package client;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import client.AppClient;
import utils.Config;

import org.junit.Before;

public class TestClient {
	AppClient client1, client2, client3;

	/** This will be processed before the Test Class is instantiated */
    @BeforeClass
    public static void BeforeClass() {
        System.out.println("@BeforeClass: TestRulesEngine");
    }

    /** This is processed/initialized before each test is conducted */
    @Before
    public void setUp() {
		System.out.println("@Before(): TestRulesEngine");
		client1 = new AppClient();
		client2 = new AppClient();
		client3 = new AppClient();
	}
	
    /** This is processed/initialized after each test is conducted */
    @After
    public void tearDown () {
		System.out.println("@After(): TestRulesEngine");
		client1 = null;
		client2 = null;
		client3 = null;
	}
	
    /** This will be processed after the Test Class has been destroyed */
    @AfterClass
    public static void afterClass () {
    	 System.out.println("@AfterClass: TestRulesEngine");
    }
    
    /** Each unit test is annotated with the @Test Annotation */
    // Test 1: one client can connect
    @Test
	public void OneConnection() {
    	System.out.println("@Test: OneConnection");
    	assertTrue(client1.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT));
    	client1.write("quit!");
	}
    
    // Test 2: multiple clients can connect
    @Test
	public void MultipltConnections() {
    	System.out.println("@Test: MultipltConnections");
    	assertTrue(client1.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT));
    	assertTrue(client2.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT));
    	assertTrue(client3.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT));
    	client1.write("quit!");
    	client2.write("quit!");
    	client3.write("quit!");
	}
    
    // Test 3: multiple client can connect and talk to the server
    @Test
	public void MultipltConnectionsTalk() {
    	System.out.println("@Test: MultipltConnectionsTalk");
    	assertTrue(client1.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT));
    	client1.write("peter");
    	assertTrue(client2.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT));
    	client2.write("jane");
    	assertTrue(client3.connect(Config.DEFAULT_HOST, Config.DEFAULT_PORT));
    	client3.write("pat");
    	
    	client1.write("quit!");
    	client2.write("quit!");
    	client3.write("quit!");
	}
    
}