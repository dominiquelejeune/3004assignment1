/**
 * 
 * @author Howard Scott Needham
 * @Version 1.0
 * 
 * 1: open a server side socket/port and establish a connection 	(Constructor)
 * 2:	start the servers main thread to listen for client request 	(start/run methods)
 * 3:	The run method is a thread that listens for client requests. When a client
 * 	requests a connection it starts a new server thread that is dedicated to the
 * 	client and is then registered in the clients hashmap - addThread () method
 * 4:	The handle method is called from the servers client thread to handle the incoming message
 * 	Notice how we filter the message - this is where you would plug in your rules engine etc .....
 * 
 * 	The server thread listens on the assigned port and when it receives a message it passes it
 * 	back to the server for processing. It is usually best practice to handle this in a seperate
 * 	class that is dedicated to handling/processing messages
 */

package network;

import java.net.*;
import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;

import java.io.*;

import utils.Config;
import utils.Filter;
import utils.Trace;

public class AppServer implements Runnable {
	int clientCount = 0;
	private Filter filter;
	private Thread thread = null;
	private ServerSocket server = null;
	private HashMap<Integer, ServerThread> clients;

	private Logger logger = Trace.getInstance().getLogger(this);
	
	public AppServer(int port) {
		try {
			/** Set up our message filter object */
			filter = new Filter();
			
			System.out.println("Binding to port " + port + ", please wait  ...");
			Trace.getInstance().write(this, "Binding to port " + port);
			logger.info("Binding to port " + port);
			/**
			 * I use a HashMap to keep track of the client connections and their
			 * related thread
			 */
			clients = new HashMap<Integer, ServerThread>();

			/** Establish the servers main port that it will listen on */
			server = new ServerSocket(port);
			/**
			 * Allows a ServerSocket to bind to the same address without raising
			 * an "already bind exception"
			 */
			server.setReuseAddress(true);
			start();
		} catch (IOException ioe) {
			Trace.getInstance().exception(ioe);
			logger.fatal(ioe);
		}
	}

	/** Now we start the servers main thread */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
			Trace.getInstance().write(this, "Server started: " + server, thread.getId());
			logger.info(String.format("Server started: %s %d", server,thread.getId()));
		}
	}

	/** The main server thread starts and is listening for clients to connect */
	public void run() {
		while (thread != null) {
			try {
				Trace.getInstance().write(this, "Waiting for a client ...");
				logger.info("Waiting for a client ...");
				addThread(server.accept());
			} catch (IOException e) {				
				Trace.getInstance().exception(this,e);
			}}
	}

	/** 
	 * Client connection is accepted and now we need to handle it and register it 
	 * and with the server | HashTable 
	 **/
	private void addThread(Socket socket) {
		Trace.getInstance().write(this, "Client accepted", socket);
		if (clientCount < Config.MAX_CLIENTS) {
			try {
				/** Create a separate server thread for each client */
				ServerThread serverThread = new ServerThread(this, socket);
				/** Open and start the thread */
				serverThread.open();
				serverThread.start();
				clients.put(serverThread.getID(), serverThread);
				this.clientCount++;
				
				// I added this * *  * * * * *  * ** *
				serverThread.send(String.format("Client %d connected to server %s:%s\n", 
						serverThread.getID(), "xxx", "yyy"));
				
			} catch (IOException e) {
				Trace.getInstance().exception(this,e);
			}
		} else {
			logger.info(String.format("Client Tried to connect: %s", socket));
			logger.info(String.format("Client refused: maximum number of clients reached: d", Config.MAX_CLIENTS));

			Trace.getInstance().write(this, "Client Tried to connect", socket );
			Trace.getInstance().write(this, "Client refused: maximum number of clients reached", Config.MAX_CLIENTS );
		}
	}

	public synchronized void handle(int ID, String input) {
		if (input == null) return;
		if (input.equals("quit!")) 
		{
			logger.info(String.format("Removing Client: %d", ID));
			Trace.getInstance().write(this,"Removing Client:", ID);
			if (clients.containsKey(ID)) {
				clients.get(ID).send("quit!" + "\n");
				remove(ID);
			}}
		else

		if (input.equals("shutdown!")) { shutdown(); }

		else 
		{
			ServerThread from = clients.get(ID);
			for (ServerThread to : clients.values()) {
				if (to.getID() != ID) {
					
					/** this is were you would hook in your rules engine */
					if (!filter.filter(from, input)) {
						from.send("This is an animal free chat service\n");
						from.send("Please refrain from refering to any form of animals\n");
					}
					else {
						to.send(String.format("%5d: %s\n", ID, input));
						logger.info(String.format("Sending Message from %s:%d to %s:%d: ",from.getSocketAddress(),from.getID(), to.getSocketAddress(), to.getID(), input));
						Trace.getInstance().logchat(this, clients.get(ID), to, input);
				}}
		}}
	}

	/** Try and shutdown the client cleanly */
	public synchronized void remove(int ID) {
		if (clients.containsKey(ID)) {
			ServerThread toTerminate = clients.get(ID);
			clients.remove(ID);
			clientCount--;

			toTerminate.close();
			toTerminate = null;
		}
	}

	/** Shutdown the server cleanly */
	public void shutdown() {
		Set<Integer> keys = clients.keySet();

		if (thread != null) {
			thread = null;
		}

		try {
			for (Integer key : keys) {
				clients.get(key).close();
				clients.put(key, null);
			}
			clients.clear();
			server.close();
		} catch (IOException e) {
			Trace.getInstance().exception(this,e);
		}
		logger.info(String.format("Server Shutdown cleanly %s", server));
		Trace.getInstance().write(this, "Server Shutdown cleanly", server );
		Trace.getInstance().close();
	}
	
}
