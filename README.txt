All six client tests work fine as long as you type them in to the console while running different
clients but the tests in the file don"t work properly because I couldn't get them to wait for the server 
response before proceeding to the next command. I've written the first three and they pass in the
fact that they connect to the server but they aren't able to receive a response from the server because 
they are removed and cleaned up right away.